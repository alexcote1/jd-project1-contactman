<?php
	$inData = getRequestInfo();

	$userId = $inData["username"];
	$email = $inData["email"];
	$phone = $inData["phone"];
	$firstName = $inData["fname"];
	$lastName = $inData["lname"];
	$address = $inData["address"];
	$oldEmail = $inData["oldEmail"];
	$oldPhone = $inData["oldPhone"];
	$oldFirstName = $inData["oldFname"];
	$oldLastName = $inData["oldLname"];
	$oldAddress = $inData["oldAddress"];

	$conn = new mysqli("localhost:3306", "newuser", "password", "site");

	if ($conn->connect_error)
	{
		returnWithError( $conn->connect_error );
	}
	else
	{
		$dupCheck = "SELECT assoc_user, con_email, con_phone, con_fname, con_lname, con_addr FROM contacts where assoc_user='" . $userId . "' and con_email='" . $email . "' and con_phone='" . $phone . "' and con_fname='" . $firstName . "' and con_lname='" . $lastName . "' and con_addr='" . $address . "'";
		$dupResult = $conn->query($dupCheck);

		if ($dupResult->num_rows > 0)
	    {
	        returnWithError("Contact Update Failed: This contact already exists!");
	    }

		else
		{
            $sql = "UPDATE contacts SET con_email='" . $email . "' , con_phone='" . $phone . "' , con_fname='" . $firstName . "' , con_lname='" . $lastName . "' , con_addr='" . $address . "' WHERE assoc_user='" . $userId . "' AND con_email = '" . $oldEmail . "' AND con_phone = '" . $oldPhone . "' AND con_fname = '" . $oldFirstName . "' AND con_lname = '" . $oldLastName . "' AND con_addr = '" . $oldAddress . "'";
			if( $result = $conn->query($sql) != TRUE )
			{
				returnWithError( $conn->error );
			}

			else
			{
				returnWithInfo( $userId, $email, $phone, $firstName, $lastName, $address);
			}
		}
		$conn->close();
	}

	function getRequestInfo()
	{
		return json_decode(file_get_contents('php://input'), true);
	}

	function sendResultInfoAsJson( $obj )
	{
		header('Content-type: application/json');
		echo $obj;
	}

	function returnWithError( $err )
	{
		$retValue = '{"error":"' . $err . '"}';
		sendResultInfoAsJson( $retValue );
	}

	function returnWithInfo( $userId, $email, $phone, $firstName, $lastName, $address)
	{
	    session_start();
	    $_SESSION["username"]=$userId;
	    $_SESSION["fname"]=$firstName;
	    $_SESSION["lname"]=$lastName;
	    $_SESSION["email"]=$email;
	    $_SESSION["address"]=$address;
		$_SESSION["phone"]=$phone;


	    $retValue = '{"username":"' . $userId . '","email":"' . $email . '","phone":"'. $phone . '","fname":"' . $firstName . '","lname":"' . $lastName . '","address":"' . $address . '","error":""}';
        sendResultInfoAsJson( $retValue );
	}

?>
