<?php

	$inData = getRequestInfo();

  $searchResults = "";
  $searchCounter = 0;

	$conn = new mysqli("localhost:3306", "newuser", "password", "site");

	if ($conn->connect_error)
	{
		returnWithError($conn->connect_error);
	}
	else
	{
      $sql = "select con_email, con_phone, con_fname, con_lname, con_addr from contacts where con_fname like '%" . $inData["searchName"] . "%' and assoc_user='" . $inData["username"] . "'";

      $result = $conn->query($sql);

      if ($result->num_rows > 0)
			{
				while ($row = $result->fetch_assoc())
				{

					if ($searchCounter > 0)
					{
						$searchResults .= ",";
					}

					$searchCounter++;
					//$searchResults .= '"' . 'FirstName: ' . $row["con_fname"] . ', LastName: ' . $row["con_lname"] . ', PhoneNumber: ' . ($row["con_phone"]) . ', Email: ' . $row["con_email"] . ', Address: ' . $row["con_addr"] . '"';
                    
                    $searchResults .= '{"firstName": "' . $row["con_fname"] . '", "lastName": "' . $row["con_lname"] . '", "phone": "' . $row["con_phone"] . '", "email": "' . $row["con_email"] . '", "address": "' . $row["con_addr"] . '"}';
				}
		}

		// $searchResults = getResults(result);

		if($searchResults == "")
		{
			returnWithError("No results");
		}

		else
		{
			//echo $searchResults;
			returnWithInfo($searchResults);
		}

		$conn->close();
	}

	// function getResults($result)
	// {
		// $results = "";
		// $counter = 0;

		// if ($result->num_rows > 0)
		// {
			// while ($row = $result->fetch_assoc())
			// {
				// if ($counter > 0)
				// {
					// $results .= ",";
				// }

				// $counter++;
				// $results .= '"' . $row["con_fname"] . ' ' . $row["con_lname"] . ' ' . ($row["con_phone"]) . ' ' . $row["con_email"] . ' ' . $row["con_address"] . '"';

			// }
		// }

		// return $results;
	// }

	function getRequestInfo()
	{
		return json_decode(file_get_contents('php://input'), true);
	}

	function sendResultInfoAsJson( $obj )
	{
		header('Content-type: application/json');
		echo $obj;
	}

	function returnWithError( $err )
	{
		$retValue = '{"error":' .$err . '"}';
		sendResultInfoAsJson( $retValue );
	}

	function returnWithInfo( $searchResults)
	{
		$retValue = '{"result":[' . $searchResults . '], "error":""}';
		sendResultInfoAsJson( $retValue );
	}
?>
