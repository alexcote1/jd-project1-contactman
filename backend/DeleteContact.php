<?php

		$inData = getRequestInfo();

		$conn = new mysqli("localhost:3306", "newuser", "password", "site");

		if($conn->connect_error)
		{
				returnWithError($conn->connect_error);
		}
		else
		{
				$dupCheck = "SELECT assoc_user, con_email, con_phone, con_fname, con_lname, con_addr FROM contacts where assoc_user = '". $inData["username"] . "' and con_email='". $inData["email"] . "' and con_phone='". $inData["phone"] . "' and con_fname='". $inData["fname"] . "' and con_lname='". $inData["lname"] . "' and con_addr = '". $inData["address"] . "'";
				$dupResult = $conn->query($dupCheck);

				if ($dupResult->num_rows == 0)
				{
					returnWithError("No Record Found");
				}

				else
				{

					$sql = "DELETE FROM contacts where assoc_user = '". $inData["username"] . "' and con_email='". $inData["email"] . "' and con_phone='". $inData["phone"] . "' and con_fname='". $inData["fname"] . "' and con_lname='". $inData["lname"] . "' and con_addr = '". $inData["address"] . "'";
					if($conn->query($sql) == true)
					{
							$id = $inData["username"];
							$firstname = $inData["fname"];
							$lastname = $inData["lname"];
							$phone = $inData["phone"];
							$email = $inData["email"];
							$address = $inData["address"];
							returnWithInfo($id, $firstname, $lastname, $phone, $email, $address);
					}
				}

				$conn->close();
		}

	function getRequestInfo()
	{
		return json_decode(file_get_contents('php://input'), true);
	}

	function sendResultInfoAsJson( $obj )
	{
		header('Content-type: application/json');
		echo $obj;
	}

	function returnWithError( $err )
	{
		$retValue = '{"id":0,"fname":"","lname":"", "username":"", "passhash":"", "error":"' . $err . '"}';
		sendResultInfoAsJson( $retValue );
	}

	function returnWithInfo($id, $firstname, $lastname, $phone, $email, $address)
	{
		session_start();
		$_SESSION["username"]=$id;
		$_SESSION["fname"]=$firstname;
		$_SESSION["lname"]=$lastname;
		$_SESSION["phone"]=$phone;
		$_SESSION["email"]=$email;
		$_SESSION["address"]=$address;

		$retValue = '{ "username":"' . $id . '","email":"' . $email . '","phone":"'. $phone . '","firstName":"' . $firstname . '","lastName":"' . $lastname . '","address":"' . $address . '", "error":""}';
		sendResultInfoAsJson( $retValue );
	}
?>