<?php

	$inData = getRequestInfo();

	$userId = inData["username"];

	$conn = new mysqli("localhost:3306", "newuser", "password", "site");

	if ($conn->connect_error)
	{
		returnWithError($conn->connect_error);
	}

	else
	{
		$id = "SELECT id FROM users WHERE userId = ." . $userName . "";
		$result =conn -> query($id); 
		if ($result->num_rows > 0)
		{
			$row = $result->fetch_assoc();
			$firstName = $row["fname"];
			$lastName = $row["lname"];
			$phone = $row["phone"];
			$address = $row["address"];
			$email = $email["email"];

			returnWithInfo($firstName, $lastName, $phone, $email, $address);
		}
		else
		{
			returnWithError("No user found");
		}

		$conn->close();
	}

	function getRequestInfo()
	{
		return json_decode(file_get_contents('php://input'), true);
	}

	function sendResultInfoAsJson( $obj )
	{
		header('Content-type: application/json');
		echo $obj;
	}

	function returnWithError( $err )
	{
		$retValue = '{"error":"' . $err . '"}';
		sendResultInfoAsJson( $retValue );
	}

	function returnWithInfo( $userId, $email, $phone, $firstName, $lastName, $address)
	{
		session_start();
	  	$_SESSION["username"]=$userId;
	  	$_SESSION["fname"]=$firstName;
	  	$_SESSION["lname"]=$lastName;
		$_SESSION["email"]=$email;
		$_SESSION["address"]=$address;
		$_SESSION["phone"]=$phone;


	  	$retValue = '{"username":"' . $userId . '","email":"' . $email . '","phone":"'. $phone . '","fname":"' . $firstName . '","lname":"' . $lastName . '","address":"' . $address . '","error":""}';
	  	sendResultInfoAsJson( $retValue );
	}
?>
