<?php

	$inData = getRequestInfo();

	$id = 0;
	$firstName = "";
	$lastName = "";
	$username = "";
	$password = "";

	$conn = new mysqli("localhost:3306", "newuser", "password", "site");
	if ($conn->connect_error)
	{
		returnWithError( $conn->connect_error );
	}
	else
	{
		$options = [
			'salt' => "sadsmnjadssssssssnsajdn",
		];
		$password = password_hash($inData["password"], PASSWORD_BCRYPT,$options);

		$sql = "SELECT fname, lname, username, passhash FROM users where username='" . $inData["username"] . "' and passhash='" . $password . "'";
		$result = $conn->query($sql);
		
		if ($result->num_rows > 0)
		{
			$row = $result->fetch_assoc();
			$firstName = $row["fname"];
			$lastName = $row["lname"];
			$username = $row["username"];

			returnWithInfo($firstName, $lastName, $username, $password );
		}
		else
		{
			returnWithError( "No Records Found" );
		}
		$conn->close();
	}

	function getRequestInfo()
	{
		return json_decode(file_get_contents('php://input'), true);
	}

	function sendResultInfoAsJson( $obj )
	{
		header('Content-type: application/json');
		echo $obj;
	}

	function returnWithError( $err )
	{
		$retValue = '{"id":0,"fname":"","lname":"", "username":"", "passhash":"", "error":"' . $err . '"}';
		sendResultInfoAsJson( $retValue );
	}

	function returnWithInfo( $firstName, $lastName, $username, $password )
	{
		session_start();
		$_SESSION["username"]=$username;
		$_SESSION["firstName"]=$firstName;
		$_SESSION["lastName"]=$lastName;
		$retValue = '{ "firstName":"' . $firstName . '","lastName":"' . $lastName . '","username":"'. $username . '","password":"' . $password . '", "error":""}';
		sendResultInfoAsJson( $retValue );
	}

?>
