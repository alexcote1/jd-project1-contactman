<?php

$inData = getRequestInfo();

$firstName = "";
$lastName = "";
$username = "";
$password = "";
$sql = "";
$dupCheck = "";

$conn = new mysqli("localhost:3306", "newuser", "password", "site");

if($conn->connect_error)
{
  returnWithError($conn->connect_error);
}

else
{
  $dupCheck = "SELECT username FROM users where username='" . $inData["username"] . "'";
  $result = $conn->query($dupCheck);

  if ($result->num_rows > 0)
  {
    returnWithError("Account Creation Failed: Username already exists!");
  }

  else
  {

      $options = [
          'salt' => "sadsmnjadssssssssnsajdn",
      ];
      $password = password_hash($inData["password"], PASSWORD_BCRYPT,$options);
  	$sql = "INSERT INTO users (username, passhash, fname, lname) VALUES('" . $inData["username"] . "', '" . $password . "', '" . $inData["firstName"] . "', '" . $inData["lastName"] . "')";

  	if($conn->query($sql) === TRUE)
  	{
      //$row = $result->fetch_assoc();

    	$firstName = $inData["firstName"];
    	$lastName = $inData["lastName"];
    	$username = $inData["username"];
      // $password = $inData["password"];

    	returnWithInfo($username, $password, $firstName, $lastName);
  	}

  	else
  	{
    	returnWithError("Account Creation Failed!");
  	}
  }
  $conn->close();
}


function getRequestInfo()
{
  return json_decode(file_get_contents('php://input'), true);
}

function sendResultInfoAsJson( $obj )
{
  header('Content-type: application/json');
  echo $obj;
}

function returnWithError( $err )
{
  $retValue = '{"firstName":"","lastName":"","username":"","password":"","error":"' . $err . '"}';
  sendResultInfoAsJson( $retValue );
}

function returnWithInfo( $username, $password, $firstName, $lastName)
{
  session_start();
  $_SESSION["username"]=$username;
  $_SESSION["firstName"]=$firstName;
  $_SESSION["lastName"]=$lastName;

  $retValue = '{"firstName":"' . $firstName . '","lastName":"' . $lastName . '","username":"'. $username . '","password":"' . $password . '","error":""}';
  sendResultInfoAsJson( $retValue );
}

?>
