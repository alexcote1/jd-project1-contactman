var urlBase = 'https://refined-bondless.aleccoder.space/';
var extension = 'php';

var id = 0;
var firstName = "";
var lastName = "";
var oldFirstName = "";
var oldLastName = "";
var oldPhone = "";
var oldEmail = "";
var oldPhone = "";


function doLogin()
{
	id = 0;
	firstName = "";
	lastName = "";

	var login = document.getElementById("loginUsername").value;
	var password = document.getElementById("loginPassword").value;

	document.getElementById("loginResult").innerHTML = "";

    if (!username || username.length === 0 || !password || password.length === 0)
	{
	document.getElementById("loginResult").innerHTML = "Please enter a username and password";
    document.getElementById('loginResult').style.color = 'red';
    return;
	}

	var jsonPayload = '{"username" : "' + login + '", "password" : "' + password + '"}';
	var url = urlBase + '/Login.' + extension;

	var xhr = new XMLHttpRequest();
	xhr.open("POST", url, false);
	xhr.setRequestHeader("Content-type", "application/json; charset=UTF-8");
	try
	{
		xhr.send(jsonPayload);

		var jsonObject = JSON.parse( xhr.responseText );

		id = jsonObject.id;

		if( id < 1 )
		{
			document.getElementById("loginResult").innerHTML = "User/Password combination incorrect";
			document.getElementById('loginResult').style.color = 'red';
			return;
		}

		firstName = jsonObject.firstName;
		lastName = jsonObject.lastName;

		setCookie("username", login);
		setCookie("id", id);

		window.location.href = "contacts.html";
	}
	catch(err)
	{
		document.getElementById("loginResult").innerHTML = err.message;
		document.getElementById('loginResult').style.color = 'red';
	}

}

function displayUsername(){
    var username = getCookie("username");
    document.getElementById("loggedInUsername").innerHTML = username;
}

function registerUser()
{
    var firstName = document.getElementById("firstName").value;
    var lastName = document.getElementById("lastName").value;
    var username = document.getElementById("username").value;
	var password = document.getElementById("password").value;

	document.getElementById("registerResult").innerHTML = "";
	
	if (!firstName || firstName.length === 0 || !lastName || lastName.length === 0 || !username || username.length === 0 || !password || password.length === 0)
	{
    	document.getElementById("registerResult").innerHTML = "Please fill out all fields";
        document.getElementById('registerResult').style.color = 'red';
        return;
	}

	var jsonPayload = '{"firstName" : "' + firstName + '", "lastName" : "' + lastName + '", "username" : "' + username + '", "password" : "' + password + '"}';
	var url = urlBase + '/Register.' + extension;

	var xhr = new XMLHttpRequest();
	xhr.open("POST", url, true);
	xhr.setRequestHeader("Content-type", "application/json; charset=UTF-8");
	try
	{
	    xhr.onreadystatechange = function()
		{
		    if (this.readyState == 4 && this.status == 200)
			{
                document.getElementById("registerResult").innerHTML = "User has been added";
                document.getElementById('registerResult').style.color = '#00FF50';
			}
		};
		xhr.send(jsonPayload);

		setCookie("password", password);
	}
	catch(err)
	{
		document.getElementById("registerResult").innerHTML = err.message;
        document.getElementById('registerResult').style.color = 'red';
	}

  $("#firstName").val('');
  $("#lastName").val('');
  $("#username").val('');
  $("#password").val('');
  $("#registerPassRepeat").val('');

}

function setCookie(cname, cvalue) {
  document.cookie = cname + "=" + cvalue + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function addContact()
{
	var username = getCookie("username");
    var fname = document.getElementById("firstName").value;
    var lname = document.getElementById("lastName").value;
    var phone = document.getElementById("phone").value;
    var email = document.getElementById("email").value;
    var address = document.getElementById("address").value;
	var contactId = 0;

	document.getElementById("contactAddResult").innerHTML = "";
	
    if (!fname || fname.length === 0 || !lname || lname.length === 0 || !phone || phone.length === 0 || !email || email.length === 0 || !address || address.length === 0)
	{
    	document.getElementById("contactAddResult").innerHTML = "Please fill out all fields.";
        document.getElementById('contactAddResult').style.color = 'red';
        return;
	}


	
	var emailCheck = $("#email").val();
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if(emailCheck.match(mailformat)){
	    $('#addWarning').hide();
	}
	else{
	    document.getElementById("addWarning").innerHTML = ("Invalid Email!");
	    document.getElementById('addWarning').style.color = 'red';
	    $('#addWarning').show();
	    return;
	}
	
	var phoneno = $("#phone").val();
	var phoneFormat = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
	if(phoneno.match(phoneFormat))
	{
		$('#phoneWarning').hide();
	}
	else
	{
		document.getElementById("phoneWarning").innerHTML = ("Invalid Phone Number!");
		document.getElementById('phoneWarning').style.color = 'red';
		$('#phoneWarning').show();
		return;
	}
	
	var jsonPayload = '{"username" : "' + username + '", "email" : "' + email + '", "phone" : "' + phone + '", "fname" : "' + fname +'", "lname" : "' + lname + '", "address" : "' + address +'"}';
	var url = urlBase + '/AddContact.' + extension;

	var xhr = new XMLHttpRequest();
	xhr.open("POST", url, true);
	xhr.setRequestHeader("Content-type", "application/json; charset=UTF-8");
	try
	{
		xhr.onreadystatechange = function()
		{
			if (this.readyState == 4 && this.status == 200)
			{
				document.getElementById("contactAddResult").innerHTML = "Contact has been added";
				document.getElementById('contactAddResult').style.color = '#00FF50';
				searchContact();
			}
		};
		xhr.send(jsonPayload);
		
		/*
		var jsonObject = JSON.parse( xhr.responseText );

		contactID = jsonObject.id;

		setCookie("contactID", contactID);*/

	}
	catch(err)
	{
		document.getElementById("contactAddResult").innerHTML = err.message;
	}

	$("#firstName").val('');
    $("#lastName").val('');
    $("#phone").val('');
     $("#email").val('');
    $("#address").val('');
}

function addModalMessageClear() {
    document.getElementById("contactAddResult").innerHTML = "";
}


function updateContact()
{
	var username = getCookie("username");
    var fname = document.getElementById("editFirstName").value;
    var lname = document.getElementById("editLastName").value;
	var phone = document.getElementById("editPhone").value;
    var email = document.getElementById("editEmail").value;
    var address = document.getElementById("editAddress").value;

	document.getElementById("updateResult").innerHTML = "";
	
	if (!fname || fname.length === 0 || !lname || lname.length === 0 || !phone || phone.length === 0 || !email || email.length === 0 || !address || address.length === 0)
	{
    	document.getElementById("updateResult").innerHTML = "Please fill out all fields.";
        document.getElementById('updateResult').style.color = 'red';
        return;
	}
	
	var emailCheck = $("#editEmail").val();
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if(emailCheck.match(mailformat)){
	    $('#editAddWarning').hide();
	}
	else{
	    document.getElementById("editAddWarning").innerHTML = ("Invalid Email!");
	    document.getElementById('editAddWarning').style.color = 'red';
	    $('#editAddWarning').show();
	    return;
	}
	
	var phoneno = $("#editPhone").val();
	var phoneFormat = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
	if(phoneno.match(phoneFormat))
	{
		$('#editPhoneWarning').hide();
	}
	else
	{
		document.getElementById("editPhoneWarning").innerHTML = ("Invalid Phone Number!");
		document.getElementById('editPhoneWarning').style.color = 'red';
		$('#editPhoneWarning').show();
		return;
	}


	var jsonPayload = '{"username" : "' + username + '", "email" : "' + email + '", "phone" : "' + phone + '", "fname" : "' + fname +'", "lname" : "' + lname + '", "address" : "' + address +'", "oldFname" : "' + oldFirstName + '", "oldLname" : "' + oldLastName + '", "oldPhone" : "' + oldPhone + '", "oldEmail" : "' + oldEmail + '", "oldAddress" : "' + oldAddress + '"}';
	var url = urlBase + '/UpdateContact.' + extension;

	var xhr = new XMLHttpRequest();
	xhr.open("POST", url, true);
	xhr.setRequestHeader("Content-type", "application/json; charset=UTF-8");
	try
	{
		xhr.onreadystatechange = function()
		{
			if (this.readyState == 4 && this.status == 200)
			{
				document.getElementById("updateResult").innerHTML = "Contact has been updated";
				document.getElementById('updateResult').style.color = '#00FF50';
				searchContact();
			}
		};
		xhr.send(jsonPayload);
	}
	catch(err)
	{
	}
	
		$("#editFirstName").val('');
        $("#editLastName").val('');
        $("#editPhone").val('');
        $("#editEmail").val('');
        $("#editAddress").val('');

}



function deleteContact(firstName, lastName, phone, email, address)
{
  var username = getCookie("username");
  var areYouSure = confirm("Are you sure you want to delete this contact?")
  
  if (areYouSure == true)
  {
	document.getElementById("deleteResult").innerHTML = "";

	var jsonPayload = '{"username" : "' + username + '", "fname" : "' + firstName + '", "lname" : "' + lastName + '", "phone" : "' + phone +'", "email" : "' + email + '", "address" : "' + address +'"}';
	var url = urlBase + '/DeleteContact.' + extension;

	var xhr = new XMLHttpRequest();
	xhr.open("POST", url, true);
	xhr.setRequestHeader("Content-type", "application/json; charset=UTF-8");
	try
	{
	    xhr.onreadystatechange = function()
		{
		    if (this.readyState == 4 && this.status == 200)
			{
			    searchContact();
				document.getElementById("deleteResult").innerHTML = "Contact has been deleted";
			    document.getElementById('deleteResult').style.color = '#00FF50';
			}
		};
		xhr.send(jsonPayload);
	}
	catch(err)
	{
	}
  }
  else {
      return;
  }
}


function searchContact()
{
    var username = getCookie("username");
	var searchName = document.getElementById("searching").value;
	document.getElementById("contactSearchResult").innerHTML = "";
	var $contactTemplate = $('#contactInfoTemplate').html();

	var contactList = document.getElementById("contactList");
	$(contactList).empty();

	document.getElementById("deleteResult").innerHTML = "";
    
    
	var jsonPayload = '{"searchName" : "' + searchName + '", "username" : "' + username +'"}';
	var url = urlBase + '/Search.' + extension;


	var xhr = new XMLHttpRequest();
	xhr.open("POST", url, true);
	xhr.setRequestHeader("Content-type", "application/json; charset=UTF-8");
	
	try
    {
        xhr.send(jsonPayload);
        
		xhr.onreadystatechange = function()
		{
			if (this.readyState == 4 && this.status == 200)
			{
			  // If buttons are pressed quickly one after the other this is where 
			  // the second call to the function jumps back in, so clear the loop
			  $(contactList).empty();
			  var jsonObject = JSON.parse( xhr.responseText );
			   
				var numContacts = jsonObject.result.length;
               
				for( var i=0; i<numContacts; i++ )
				{
				    $('#contactList').append(Mustache.render($contactTemplate, jsonObject.result[i]));
				    
				    var buttons = document.createElement('div');
				    buttons.className = "col-md-4 col-4";
				    $('.row').append(buttons);
				    
				    var buttonsContent = document.createElement('div');
				    
				    buttonsContent.innerHTML = `
				    
				     <button type="button" class="btn btn-default" id="editConBtn${i}" data-toggle="modal" data-target="#modalEdit" onclick="editContactModal('${jsonObject.result[i].firstName}', '${jsonObject.result[i].lastName}', '${jsonObject.result[i].phone}', '${jsonObject.result[i].email}', '${jsonObject.result[i].address}');" style="margin-right: 5px;"><i class="fas fa-user-edit"></i></button>
        		    <button type="button" class="btn btn-default" id="deleteConBtn${i}" onclick="deleteContact('${jsonObject.result[i].firstName}', '${jsonObject.result[i].lastName}', '${jsonObject.result[i].phone}', '${jsonObject.result[i].email}', '${jsonObject.result[i].address}');"><i class="fas fa-trash-alt"></i></button>

				    `
				    buttons.appendChild(buttonsContent);
				    
				    if (numContacts < 1) {
						document.getElementById("contactSearchResult").innerHTML = "No contacts found";
						document.getElementById('contactSearchResult').style.color = '#00FF50';
					}
					else
					{
					    document.getElementById("contactSearchResult").innerHTML = "";
					}
				    
				}

			}

		};

		
	}
	catch(err)
	{
		document.getElementById("contactSearchResult").innerHTML = err.message;
	}

}


// Code to make registration passwords match before submitting
function checkPasswordMatch() {
    var password = $("#password").val();
    var confirmPassword = $("#registerPassRepeat").val();

    if (password != confirmPassword) {
        document.getElementById("registerResult").innerHTML = ("Passwords do not match!");
        document.getElementById('registerResult').style.color = 'red';
        return;
      }
}


$(document).ready(function () {
   $("#registerPassRepeat").keyup(checkPasswordMatch);
   // $("#searching").bindWithDelay("keypress", searchContact(), 500);
});



function editContactModal(firstName, lastName, phone, email, address) {
	oldFirstName = firstName;
    oldLastName = lastName;
    oldPhone = phone;
    oldEmail = email;
    oldAddress = address;
    
    document.getElementById("updateWarning").innerHTML = "";
    document.getElementById("updateResult").innerHTML = "";
	
	document.getElementById("editFirstName").value = firstName;
	document.getElementById("editLastName").value = lastName;
	document.getElementById("editPhone").value = phone;
	document.getElementById("editEmail").value = email;
	document.getElementById("editAddress").value = address;
}
