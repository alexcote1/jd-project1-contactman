from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return HttpResponse("Hello world, the most classic of classic tests!")

# Create your views here.
